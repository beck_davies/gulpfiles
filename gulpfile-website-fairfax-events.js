var gulp = require('gulp'),
	autoprefixer = require('gulp-autoprefixer'),
    changed = require('gulp-changed'),
    neat = require('node-neat').includePaths,
    //minifycss = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify');

var sassSrc = './static/**/scss/*.scss',
	sassDest = './static/**'
	jsSrc = './static/**/js/main.js'
	jsDest = 'static'

// SASS - compile
gulp.task('sass', function(){
 	gulp.src(sassSrc, { base: './static/**' })
        .pipe(sass({
        	outputStyle: 'expanded',
        	includePaths: ['sass'].concat(neat)
        }))
        
        //.pipe(autoprefixer('last 2 version', 'ie 8', 'ie 9', 'ios 6', 'android 4'))
        //.pipe(rename({suffix: '.min'}))
        //.pipe(minifycss())
		
		.pipe(rename(function(path){
			path.dirname += "./../css"         
		}))

        .pipe(gulp.dest(sassDest))
});

// JS - rename and minify
// gulp.task('scripts', function() {
//   gulp.src(jsSrc)
//     .pipe(rename({suffix: '.min'}))
//     .pipe(uglify())
//     .pipe(gulp.dest(jsDest));
// });

//gulp.task('default', ['sass', 'scripts'], function() {
gulp.task('default', ['sass'], function() {
  // watch for SASS changes
  gulp.watch(sassSrc, ['sass']);
	
 // watch for JS changes
 //gulp.watch(jsSrc, ['scripts']);
});


// gulp.task('default', ['sass',], function() {
//   // watch for SASS changes
//   gulp.watch(sassSrc, ['sass']);
// });

